/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstra;

/**
 *
 * @author im285503
 */
public class Rectangle extends Obstacle{
	Point p;
	int larg;
	int haut;

	public Rectangle(Point p, int larg, int haut) {
		this.p = p;
		this.larg = larg;
		this.haut = haut;
	}
	public Rectangle(Rectangle r){
		this.p=r.p;
		this.larg=r.larg;
		this.haut=r.haut;
	}
	public Point getP() {
		return p;
	}
	public int getLarg() {
		return larg;
	}
	public int getHaut() {
		return haut;
	}
	public void setP(Point p) {
		this.p = p;
	}
	public void setLarg(int larg) {
		this.larg = larg;
	}
	public void setHaut(int haut) {
		this.haut = haut;
	}
    @Override
	public boolean collision(Point p){ //renvoie true si le point est sur le rectangle
            boolean c=false;
            if (p.getX()>this.getP().getX()&&p.getY()>this.getP().getY())
                if((p.getX()-this.getP().getX()<=this.getLarg())&&(p.getY()-this.getP().getY()<=this.getHaut()))
                    c=true;
            return c;
        }
        @Override
        public String toString() {
        return "R{"+ p + ", l=" + larg + ", h=" + haut + '}';
        }
	
}
