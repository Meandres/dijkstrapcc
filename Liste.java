/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstra;

import java.util.ArrayList;

/**
 *
 * @author ilya
 */
public class Liste extends ArrayList<Sommet>{
    ArrayList<Sommet> contenu;
    
    public Liste(){
        contenu=new ArrayList<>(10);
    }
    @Override
    public int size(){
        return this.contenu.size();
    }
    @Override
    public Sommet get(int i){
        return contenu.get(i);
    }
    @Override
    public Sommet set(int i, Sommet s){
        Sommet temp=contenu.get(i);
        contenu.set(i, s);
        return temp;
    }
    @Override
    public boolean add(Sommet s){
        return contenu.add(s);
    }
    @Override
    public void clear(){
        contenu.clear();
    }
    public void remove(Sommet s){
        contenu.remove(s);
    }
    public Liste copie(){
        Liste cpsommets=new Liste();
        for(Sommet som: this.contenu){
           cpsommets.add(new Sommet(som));
        }
        return cpsommets;
    }
    public int indexOf(Sommet s){
        return contenu.indexOf(s);
    }
    public boolean isEmpty(){
        return contenu.isEmpty();
    }
    public int trouve(Sommet s){
        int i=0;
        while((i<=this.size()-1)&&(this.get(i)!=null)&&(!this.get(i).p.equals(s.p))){
            i++;
		}
        if(i>=this.size())
            i=-1;
        return i;
    }
    public Sommet min(){
        Sommet s=this.get(0);
        for(int i=0; i<this.size();i++){
            if(this.get(i).getDis()<s.getDis()){
                s=this.get(i);
            }
        }
        return s;
    }
    public void tasserD(int i){
        if(2*i>=this.size()){
        }
        else{
            if(2*i==this.size()-1){//juste un enfant gauche
                if(this.get(2*i).getDis()<this.get(i).getDis()){
                    inverser(this.get(2*i), this.get(i));
                    tasserD(2*i);
                }
            }
            else{//les deux enfants
                if(this.get(2*i).getDis()<this.get(2*i+1).getDis()){
                    inverser(this.get(2*i), this.get(i));
                    tasserD(2*i);
                }
                else if(this.get(2*i).getDis()>this.get(2*i+1).getDis()){
                    inverser(this.get(2*i+1), this.get(i));
                    tasserD(2*i+1);
                }
            }
        }
    }
    public void supprimer(Sommet w){
        this.inverser(w, this.get(this.size()-1));
        this.remove(w);
        this.tasserD(1);
    }
    public void tasserM(int i){
            while(i/2>=1 && this.get(i/2).getDis()>this.get(i).getDis()){  
                inverser(this.get(i/2), this.get(i));
                i/=2;
            }
        }
    public void retri(Sommet s){
        tasserM(this.indexOf(s));
    }
    public void inverser(Sommet s1, Sommet s2){
        int temp=this.indexOf(s2);
        this.set(this.indexOf(s1), s2);
        this.set(temp, s1);
    }
    public void inverser(int s1, int s2){
        Sommet temp=this.get(s2);
        this.set(s2, this.get(s1));
        this.set(s1, temp);
    }
    public void afficher(){
        for(Sommet som: this.contenu){
            if(som!=null){
                System.out.println(som.p+" dis : "+som.getDis());
            }
            else
                System.out.println("null");
        }
        System.out.println();
    }
    @Override
    public String toString() {
        return contenu.toString();
    }
    
}

