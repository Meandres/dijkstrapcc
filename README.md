# DijkstraPCC

Une implementation de l'algo du Plus Court Chemin de Dijkstra : [https://fr.wikipedia.org/wiki/Algorithme_de_Dijkstra](url).
Dans un espace 2D encombré, on veut trouver le plus court chemin entre le point en bas a gauche de la scene et celui en haut a droite.
L'IMH fixe la taille de la fenetre à 800x600.
Le but du projet est de faire des améliorations et des optimisations.
Le depart est D et l'arrivée A.
L'archive est celle du dossier créé par NetBeans (si vous voulez importer directement le projet).

1. Dijkstra par tas
Pour accelerer la recherche du sommet ayant la distance minimale, on utilise un tas-min.
2. Raffinement Itératif
Pour affiner le dessin, on fait une première fois l'algo de Dijkstra sur des points générés pseudo-aléatoirement. Pour chaque point P on regarde si la distance DP+PA est inférieure à un certain rapport (1.010 puis 1.005) et on le conserve pour la suite si c'est le cas (le premier dijkstra nous donne la distance DP mais il faut refaire un dijkstra "dans l'autre sens" pour avoir la distance PA).
3. 2D-arbre (pas encore fait)
Pour accelerer la recherche de voisins, on met les sommets dans un 2D-arbre.